# L_ile Interdite, par D'Almeida Yannick et Bastien Repaire

L'ile interdite est un projet que nous devions effectuer dans le cadre du cours de POGL. Il s'agit d'un jeu de plateau représentant dans lequel les joueurs (au nombre de un à quatre) débutant à l'héliport (situé au centre du plateau) doivent se déplacer afin de récuperer des clés de différents éléments pour ensuite récupérer les artéfactes de ces mêmes élements, avant que les cases et artéfacts et joueurs soient submergées.

Ainsi, à cet effet, nous sommes partis du Conway comme base. Nous avons commencé à coder les cases(vide au départ) représentant le plateau ainsi que l'héliport au centre.(dans Héliport, Cellule, CModele). Ensuite nous avons représenté les artefacts (feu, eau terre et air) dans un enumerate, puis les clés en tant que sous classe d'Artefact, tout en mettant à jour et en créant les différentes vues dont on avait besoin, les différentes méthodes... Enfin on a créé la Classe joueurs ainsi que les méthodes représentant leurs intéractions avec le plateau. Pour la méthode de déplacement on a choisi les clics de boutons. Comme action spéciale on a inclus le sac de sable et le retour à l'hélicoptère seul ou avec un autre joueur s'ils se trouvent à la même position. Comme point améliorable, les joueurs ne peuvent assècher que les cases sur lequels ils se trouvent, ceci est facilement améliorable.






